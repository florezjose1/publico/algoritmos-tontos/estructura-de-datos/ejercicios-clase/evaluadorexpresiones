/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Utils.EvaluadorParentesis;
import java.text.DecimalFormat;
import ufps.util.coleciones_seed.ListaCD;
import ufps.util.coleciones_seed.Pila;

/**
 * Evaluar de expresión matemática algebraica
 *
 * @author Jose Flórez
 */
public class EvaluadorExpresion {

    private String expresion;

    public EvaluadorExpresion() {
    }

    public EvaluadorExpresion(String expresion) {
        this.expresion = expresion;
    }

    public String getExpresion() {
        return expresion;
    }

    public void setExpresion(String expresion) throws Exception {
        EvaluadorParentesis eval = new EvaluadorParentesis();
        if(eval.evaluador(expresion)) {
            this.expresion = expresion;
        } else {
            throw new Exception("Expresion no válida");
        }
        
    }

    public boolean isOperator(String o) {
        return o.equals("^") || o.equals("*") || o.equals("/")
                || o.equals("+") || o.equals("-");
    }

    public boolean isOperatorCompanion(String o) {
        return o.equals("+") || o.equals("-") || o.equals("(") || o.equals("*") || o.equals("/") || o.equals("^");
    }

    public boolean isInferior(String s, String of) {
        return s.equals("^") && (of.equals("*") || of.equals("/") || of.equals("+") || of.equals("-"))
                || s.equals("*") && (of.equals("/") || of.equals("+") || of.equals("-"))
                || s.equals("/") && (of.equals("+") || of.equals("-"))
                || s.equals("+") && of.equals("-");

    }

    private boolean isParenthesis(String o) {
        return o.equals("(") || o.equals(")");
    }

    /**
     * Busca el numero dentro de la expresion
     *
     * @param e: expresion a evaluar
     * @param p: posición a tener en cuenta para buscar
     * @param n: string número a buscar
     */
    private String buscarNumero(String e, int p, String n) {
        while (p < e.length()) {
            String v = e.charAt(p) + "";
            if (!this.isOperator(v) && !this.isParenthesis(v)) {
                n += v;
            } else {
                return n;
            }
            p++;
        }
        return n;
    }

    /**
     * Separamos la expresion en valores unitarios o tokens unarios para
     * realizar dichas operaciones
     *
     * @param e: La expresiona a evaluar
     * @param cola: Donde asignamores cada una de las expresiones encontradas
     */
    private void separarValores(String e, ListaCD<String> terminos) {
        int i = 0, limit = e.length();
        String ant = "";
        while (i < limit) {
            String v = e.charAt(i) + "";
            if (this.isParenthesis(v)) {
                terminos.insertarFinal(v);
                ant = v;
            } else if (this.isOperator(v)) {
                // Si el anterior también fue un operador, significa que la posición {i}
                // es un operador acompañante
                if (this.isOperatorCompanion(ant) || i == 0) {
                    /**
                     * Al ser un operador acompañante buscamos su siguiente
                     * Cuando encontramos un operador buscamos en la siguiente
                     * posición para determinar si tenemos el siguiente caso:
                     *
                     * 2*3 ó 2 * -3
                     */
                    String n = v + "";
                    n = this.buscarNumero(e, i + 1, n);
                    terminos.insertarFinal(n);
                    ant = n;
                    i = i + n.length() - 1;
                } else {
                    terminos.insertarFinal(v);
                    ant = v;
                }
            } else {
                String n = v + "";
                n = this.buscarNumero(e, i + 1, n);
                terminos.insertarFinal(n);
                ant = n;
                i = i + n.length() - 1;
            }
            i++;
        }
    }

    /**
     * Verifica si el símbolo pertenece a uno de apertura
     *
     * @param s, simbolo a verificar
     * @return true si pertenece al requerido o false si no.
     */
    private boolean symbolParenthesisOpen(String s) {
        return s.equals("(") || s.equals("{") || s.equals("[");
    }

    /**
     * Verifica si el símbolo pertenece a uno de cierre
     *
     * @param s, simbolo a verificar
     * @return true si pertenece al requerido o false si no.
     */
    private boolean symbolParenthesisClose(String s) {
        return s.equals(")") || s.equals("}") || s.equals("]");
    }

    /**
     * Verifica su par, es decir, cada símbolo de apertura debe tener su cierre
     *
     * @param v: Valor o símbolo de apertura
     * @param c: Valor o símbolo de cierre
     * @return true si v equivale a uno de apertura y c a su respectivo
     * cierre... o false si no.
     */
    public boolean hisPair(String v, String c) {
        return (v.equals("(") && c.equals(")"))
                || (v.equals("{") && c.equals("}"))
                || (v.equals("[") && c.equals("]"));
    }

    /**
     * Intercambia los valores prefijo de i y i-1
     *
     * @param l: lista de valores
     * @param i: posición de interación donde es llamada
     * @param v: valor a intercambiar con i-1
     */
    private void interchangesValuesPre(ListaCD<String> l, int i, String v) {
        // Como no hay nada en las anteriores posiciones a tener en cuenta, entonces solo intercambia
        String v_ant = l.eliminar(i - 1);
        l.insertarFinal(v);
        l.insertarFinal(v_ant);
    }

    /**
     * Intercambia los valores posfijo de i y i+1
     *
     * @param l: lista de valores
     * @param i: posición de interación donde es llamada
     * @param v: valor a intercambiar con i-1
     */
    private void interchangeValuesPos(ListaCD<String> l, int i, String v) {
        String v_ant = l.eliminar(0);
        l.insertarInicio(v);
        l.insertarInicio(v_ant);
    }

    /**
     * Inserta lista temporal en lista global una vez buscados las previas
     * ordenaciones de operadores
     *
     * @param l: lista de valores original
     * @param ltemp: lista temporal a seguir después de la original
     * @param v: valor de iteración
     */
    private void insertListInListPre(ListaCD<String> l, ListaCD<String> ltemp, String v) {
        l.insertarFinal(v);
        // E insertamos nuestra ltemporal
        int k = ltemp.getTamano();
        // inserto mi lista temporal nuevamente a la global
        while (k > 0) {
            k--;
            l.insertarFinal(ltemp.eliminar(0));
        }
    }

    /**
     * Inserta lista temporal en lista global una vez buscados las previas
     * ordenaciones de operadores
     *
     * @param l: lista de valores original
     * @param ltemp: lista temporal a seguir después de la original
     * @param v: valor de iteración
     */
    private void insertListInListPos(ListaCD<String> l, ListaCD<String> ltemp, String v) {
        // insertamos nuestra ltemporal
        int k = ltemp.getTamano();
        // inserto mi lista temporal nuevamente a la global
        while (k > 0) {
            k--;
            l.insertarFinal(ltemp.eliminar(0));
        }
        // Insertamos nuestro v que sería nuestro signo al final
        l.insertarFinal(v);
    }

    /**
     * Busca los valores de previas organizaciones o de operadores iguales o
     * mayores al actual para colocar el signo encontrado antes de ello
     *
     * @param l: lista a evaluar
     * @param i: posición de iteración
     * @param v: signo encontrado
     * @throws Exception
     */
    private void insertStartPre(ListaCD<String> l, int i, String v) throws Exception {
        String v_a1 = l.getPosInfo(i - 1), v_a2 = l.getPosInfo(i - 2);
        if (!this.isOperator(v_a1) && !this.isOperator(v_a2) && !this.symbolParenthesisClose(v_a1)) {
            /**
             * Si son diferentes a operadores indica que hay una operación
             * antes, por ello debo buscar donde empieza teniendo los posibles
             * casos:
             *
             * 1. *,2,3,+4
             *
             * 2. 2-1-*,2,3,+4
             */
            ListaCD<String> ltemp = new ListaCD();
            ltemp.insertarInicio(l.eliminar(i - 1));
            ltemp.insertarInicio(l.eliminar(i - 2));
            // Ya existen números, debo iterar hasta buscar simbolos y encontrar números nuevamente
            boolean existS = false;
            int j = l.getTamano();
            String lastSymbol = "";
            while (j > 0) {
                j--;
                String vT = l.eliminar(j); // vT = valor Temporal
                // Si es número y aún no se han encontrado simbolos
                if (!this.isOperator(vT) && !existS) {
                    ltemp.insertarInicio(vT);
                } else if (this.isOperator(vT)) {
                    existS = true;
                    lastSymbol = vT;
                    ltemp.insertarInicio(vT);
                } else {
                    // Cómo encontré nuevamente un número, este ya no cuenta por lo que vuelvo a insertarlo
                    l.insertarFinal(vT);
                    break;
                }
            }
            // Evaluando sumas, el caso 2, v = + y lastSymbol = -
            if (this.isInferior(v, lastSymbol)) {
                // Inserto nuevamente el -
                // Eliminandolo de mi lista temporal primero
                l.insertarFinal(ltemp.eliminar(0));
                // inserto el signo correspondiente a la posición i,
                // y que siguiendo el ejemplo sería +
                this.insertListInListPre(l, ltemp, v);

            } else {
                // como no es inferior, nuestra posición i quedará de primera 
                this.insertListInListPre(l, ltemp, v);
            }

        } else {
            // Como no hay nada en las anteriores posiciones a tener en cuenta, entonces solo intercambia
            if (!this.symbolParenthesisClose(v_a1)) {
                this.interchangesValuesPre(l, i, v);
            } else {
                l.insertarFinal(v);
            }
        }
    }

    /**
     * Busca los valores de previas organizaciones o de operadores iguales o
     * mayores al actual para colocar el signo encontrado antes de ello
     *
     * @param l: lista a evaluar
     * @param i: posición de iteración
     * @param v: signo encontrado
     * @throws Exception
     */
    private void insertPosFin(ListaCD<String> l, int i, String v) throws Exception {
        String v_a1 = l.getPosInfo(0), v_a2 = l.getPosInfo(1);
        if (!this.isOperator(v_a1) && !this.isOperator(v_a2)) {
            ListaCD<String> ltemp = new ListaCD();
            ltemp.insertarFinal(l.eliminar(0));
            ltemp.insertarFinal(l.eliminar(1));
            // Ya existen números, debo iterar hasta buscar simbolos y encontrar números nuevamente
            boolean existS = false;
            int j = l.getTamano();
            String lastSymbol = "";
            while (j > 0) {
                j--;
                String vT = l.eliminar(j); // vT = valor Temporal
                // Si es número y aún no se han encontrado simbolos
                if (!this.isOperator(vT) && !existS) {
                    ltemp.insertarFinal(vT);
                } else if (this.isOperator(vT)) {
                    existS = true;
                    lastSymbol = vT;
                    ltemp.insertarFinal(vT);
                } else {
                    // Cómo encontré nuevamente un número, este ya no cuenta por lo que vuelvo a insertarlo
                    l.insertarFinal(vT);
                    break;
                }
            }

            // Evaluando sumas, el caso 2, v = + y lastSymbol = -
            if (this.isInferior(v, lastSymbol)) {
                // Inserto nuevamente el -
                // Eliminandolo de mi lista temporal primero
                l.insertarFinal(ltemp.eliminar(ltemp.getTamano() - 1));
                // inserto el signo correspondiente a la posición i,
                // y que siguiendo el ejemplo sería +
                this.insertListInListPos(l, ltemp, v);

            } else {

                // insertamos nuestra ltemporal
                int k = ltemp.getTamano();
                // inserto mi lista temporal nuevamente a la global
                while (k > 0) {
                    k--;
                    l.insertarFinal(ltemp.eliminar(0));
                }
                // Insertamos nuestro v que sería nuestro signo al final
                l.insertarFinal(v);
            }
        } else {
            this.interchangeValuesPos(l, i, v);
        }
    }

    private ListaCD<String> expressionOrderingPre(ListaCD<String> e, String o, int p) throws Exception {
        ListaCD<String> l = new ListaCD();
        int i = p, limit = e.getTamano();
        while (i < limit) {
            String v = e.getPosInfo(i);
            if (this.symbolParenthesisOpen(v)) {
                l.insertarFinal("(");
                ListaCD<String> lTemp = this.expressionOrderingPre(e, o, i + 1);

                int k = lTemp.getTamano();
                i = i + k;

                while (k-- > 0) {
                    l.insertarFinal(lTemp.eliminar(0));
                }
            } else if (this.symbolParenthesisClose(v)) {
                l.insertarFinal(")");
                return l;
            } else if (v.equals(o)) {

                // Si es prefijo, se extrae el anterior de la pila para insertarlo después del signo
                // cuando encuentro el signo, obtengo el siguiente para realizar la operación correspondiente.
                String v_sig = e.getPosInfo(i + 1);

                /**
                 * Pero hay casos a tener en cuenta: si ya ha sido previamente
                 * organizada, podríamos tener dos posibles casos:
                 *
                 * 1. 2+ *,3,4
                 *
                 * 2. *,3,4 +3
                 *
                 * Para el 1: Si mi siguiente es un signo, el signo lo dejo y el
                 * 2 lo paso al final, quedando: +,*,3,4,2 o también puede ser
                 * solo intercambiando +,2,*,3,4
                 *
                 * Para el segundo, si el aterior de mi anterior es otro número,
                 * entonces debo pasar el signo al inicio, quedando: +,*3,4,3
                 */
                // Para el primer caso:
                if (this.isOperator(v_sig)) {
                    if (l.getTamano() > 2) {
                        this.insertStartPre(l, i, v);
                    } else {
                        this.interchangesValuesPre(l, i, v);
                    }
                } else {
                    /**
                     * para el caso dos tenemos algo particular: Tenemos que
                     * encontrar la punta de dichas operaciones, y como estamos
                     * indicando que está después de alguna operación, entonces
                     * tenemos:
                     *
                     * 1. Validar si mis dos anteriores son numeros - De ser
                     * así, debo buscar donde terminan e insertar el signo en
                     * esa posición y dejar el número después de todos ellos
                     *
                     * 2. Si alguno de ellos no es número, entonces solo
                     * intercambiar posición del signo
                     */
                    if (l.getTamano() > 2) {
                        this.insertStartPre(l, i, v);
                    } else {
                        // Si ya tiene un tamaño mayor a dos, entonces es porque existe 
                        // una operación antes, de lo contrario, solo indicaría que deben intercambiarse los valores
                        this.interchangesValuesPre(l, i - p + 1, v);
                    }
                }

            } else {
                l.insertarFinal(v);
            }

            i++;
        }
        return l;
    }

    private ListaCD<String> expresionOrderingPos(ListaCD<String> e, String o) throws Exception {
        ListaCD<String> l = new ListaCD();
        int i = e.getTamano();
        while (i > 0) {
            i--;
            String v = e.getPosInfo(i);
            if (v.equals(o)) {

                /**
                 * Si es posfijo: Se inserta primero el siguiente y luego el
                 * signo
                 *
                 *
                 * Al igual que en prefijo, hay cosas a tener en cuenta:
                 *
                 * 1: 2+4,3,*
                 *
                 * 2: 4,5,*+2
                 *
                 * Para el 1: Si el siguiente de mi siguiente es un número, el
                 * signo pasa al final y el dos queda ahí, quedando: 2,4,3,*,+
                 *
                 * Para el 2: Si el anterior es un signo, el signo queda al
                 * final, pero el dos pasa al inicio, quedando: 2,4,5*+
                 */
                // cuando encuentro el signo, obtengo el anterior para realizar la operación correspondiente.
                String v_sig = e.getPosInfo(i - 1);

                /**
                 * Pero hay casos a tener en cuenta: si ya ha sido previamente
                 * organizada, podríamos tener dos posibles casos:
                 *
                 * 1. 3,4,* + 3
                 *
                 * 2. 2+3,4,*
                 *
                 * Para el 1{+}: Si mi anterior es un signo, el signo lo dejo y
                 * el 3 lo paso al inicio, quedando: 3,3,4,*,+ o también puede
                 * ser solo intercambiando 3,4,*3,+
                 *
                 * Para el segundo, si el siguiente de mi siguiente es otro
                 * número, entonces debo pasar el signo al final, quedando:
                 * 2,3,4,*,+
                 */
                // Para el primer caso:
                if (this.isOperator(v_sig)) {
                    if (l.getTamano() > 2) {
                        this.insertPosFin(l, i, v);
                    } else {
                        String v_ant = l.eliminar(i - 1);
                        l.insertarInicio(v);
                        l.insertarInicio(v_ant);
                    }
                } else {
                    /**
                     * para el caso dos tenemos algo particular: Tenemos que
                     * encontrar la punta de dichas operaciones, y como estamos
                     * indicando que está después de alguna operación, entonces
                     * tenemos:
                     *
                     * 1. Validar si mis dos siguientes son numeros - De ser
                     * así, debo buscar donde terminan e insertar el signo en
                     * esa posición y dejar el número después de todos ellos
                     *
                     * 2. Si alguno de ellos no es número, entonces solo
                     * intercambiar posición del signo
                     */
                    if (l.getTamano() > 2) {
                        this.insertPosFin(l, i, v);
                    } else {
                        // Si ya tiene un tamaño mayor a dos, entonces es porque existe 
                        // una operación antes, de lo contrario, solo indicaría que deben intercambiarse los valores
                        this.interchangeValuesPos(l, i, v);
                    }
                }

            } else {
                l.insertarInicio(v);
            }
        }

        return l;
    }

    /**
     * Convierte la expresion a una cola por términos para ser procesada
     *
     * @return String de la expresion procesada
     * @throws java.lang.Exception: traspaso de excepcion
     */
    public ListaCD<String> convertirExpresion() throws Exception {
        String exp = this.expresion;
        ListaCD<String> terminos = new ListaCD();
        // separamos la expresion en valores unarios
        this.separarValores(exp, terminos);

        terminos = this.expressionOrderingPre(terminos, "^", 0);
        terminos = this.expressionOrderingPre(terminos, "*", 0);
        terminos = this.expressionOrderingPre(terminos, "/", 0);
        terminos = this.expressionOrderingPre(terminos, "+", 0);
        terminos = this.expressionOrderingPre(terminos, "-", 0);
        System.out.println("" + terminos);
        return terminos;
    }
    
    /**
     * Función que retorna la expresión algebraica en prefijo
     * @return String con la expresion convertida a prefijo
     * @throws Exception: paso de excepcion
     */
    public String getPrefijo() throws Exception {
        return this.convertirExpresion().toString();
    }

    /**
     * Función que retorna la expresión algebraíca en posfijo
     * @return String con la expresion convertida posfijo
     */
    public String getPosfijo() {
        return "En construcción";
    }

    /**
     * Ejecuta la operación de acuerdo al signo
     * @param signo: signo a operar
     * @param v1: valor uno
     * @param v2: valor dos
     * @return resultado de la operación entre valor y valor 2
     */
    private double operaciones(String signo, String v1, String v2) {
        double result = 0;
        switch (signo) {
            case "^": {
                result = Math.pow(Double.parseDouble(v1), Double.parseDouble(v2));
                break;
            }
            case "*": {
                result = Double.parseDouble(v1) * Double.parseDouble(v2);
                break;
            }
            case "/": {
                result = Double.parseDouble(v1) / Double.parseDouble(v2);
                break;
            }
            case "+": {
                result = Double.parseDouble(v1) + Double.parseDouble(v2);
                break;
            }
            case "-": {
                result = Double.parseDouble(v1) - Double.parseDouble(v2);
                break;
            }
            default:
                break;
        }

        return result;
    }

    /**
     * Función recursiva que hasta que no acabe la pila no retorna resultado
     * @param p: pila a operar
     * @param result: variable de almacenamiento
     * @return resultado de las operaciones de la pila
     */
    private double desapilar(Pila<String> p, double result) {
        Pila<String> r = new Pila();
        while (!p.esVacia()) {
            String v = p.pop();
            if (!this.isOperator(v)) {
                r.push(v);
            } else {
                result = this.operaciones(v, r.pop(), r.pop());
                r.push(result+"");
            }
        }

        if (r.getTamano() > 2) {
            return this.desapilar(r, result);
        } else {
            return result;
        }
    }

    public String getEvaluarExpresion() throws Exception {
        ListaCD<String> terminos = this.convertirExpresion();
        Pila<String> pila = new Pila();
        int i = terminos.getTamano();
        while (i > 0) {
            i--;
            String v = terminos.eliminar(0);
            if (!this.isParenthesis(v)) {
                pila.push(v);
            }
        }
        double result = 0;
        result = this.desapilar(pila, result);
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(result);
    }
}
