/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author flore
 */
public class EvaluadorParentesis {

    public EvaluadorParentesis() {
    }
    
    
    
     /**
     * Verifica si el símbolo pertenece a uno de apertura
     *
     * @param s, simbolo a verificar
     * @return true si pertenece al requerido o false si no.
     */
    private boolean symbolOpen(String s) {
        return s.equals("(") || s.equals("{") || s.equals("[");
    }

    /**
     * Verifica si el símbolo pertenece a uno de cierre
     *
     * @param s, simbolo a verificar
     * @return true si pertenece al requerido o false si no.
     */
    private boolean symbolClose(String s) {
        return s.equals(")") || s.equals("}") || s.equals("]");
    }

    /**
     * Verifica su par, es decir, cada símbolo de apertura debe tener su cierre
     *
     * @param v: Valor o símbolo de apertura
     * @param c: Valor o símbolo de cierre
     * @return true si v equivale a uno de apertura y c a su respectivo
     * cierre... o false si no.
     */
    public boolean hisPair(String v, String c) {
        return (v.equals("(") && c.equals(")"))
                || (v.equals("{") && c.equals("}"))
                || (v.equals("[") && c.equals("]"));
    }

    /**
     * Verifica si una cadena contiene la misma igualdad de paréntesis de
     * apertura y de cierre y que estén bien colocados, es decir: ([]) : Esto
     * sería válido ([}): Esto sería inválido
     *
     * @param cadena: Cadena a evaluar
     * @return true si están completos y bien colocados los paréntesis, de lo
     * contrario false
     */
    public boolean evaluador(String cadena) {
        Pila<String> p = new Pila();
        int i = 0, limit = cadena.length();
        while (i < limit) {
            String s = cadena.charAt(i) + "";

            if (symbolOpen(s)) {
                // Cada vez que se encuentra un paréntesis de apertura de ingresa a la cola
                p.push(s);
            } else if (symbolClose(s)) {
                /**
                 * Cada vez que se encuentra uno de cierre se retira el último
                 * ingresado pero solo si se cumplen las siguientes condiciones:
                 */

                /**
                 * 1. Para hacer un pop no puede estar vacía, esto es por si
                 * nuestra cadena empieza con uno de cierre no es válido
                 */
                if (p.esVacia()) {
                    return false;
                }

                /**
                 * 2. Si el último en ingresar fue un paréntesis, el primero en
                 * salir debe ser su par, de lo contrario, se retorna false, -
                 * De esta forma se asegura que no solo coincidan en la misma
                 * cantidad de apertura y de cierre sino que también sean del
                 * mismo tipo.
                 */
                String n = p.pop();
                if (!hisPair(n, s)) {
                    System.err.println("Error: " + n + ", su elemento de cierre no es " + s + ". Verifica e intenta nuevamente...");
                    return false;
                }
            }
            i++;
        }

        return p.esVacia();
    }
}
