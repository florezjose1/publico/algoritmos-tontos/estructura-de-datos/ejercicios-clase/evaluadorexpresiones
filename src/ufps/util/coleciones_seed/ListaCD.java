/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Flórez
 */
public class ListaCD<T> {

    private NodoD<T> cabecera;
    private int tamano = 0;

    public ListaCD() {
        this.cabecera = new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSiguiente(this.cabecera);
        this.cabecera.setAnterior(this.cabecera);
    }

    public int getTamano() {
        return this.tamano;
    }

    /**
     * Inserta un elemento al inicio de la lista
     *
     * @param info un objeto a ser almacenado en la lista
     */
    public void insertarInicio(T info) {
        /*
        Crear el nodo (x)
        Insertar el info (x)
        El siguiente del nodo nuevo es SIGUIENTE DE LA CABECERA (x)
        El anterior del nuevo nodo es LA CABECERA (x)
        Siguiente de cabera es el nuevo nodo (x)
        El siguiente del nuevo nodo su anterior será nuevo nodo
        Aumentar la cardinalidad 

         */

        NodoD<T> nuevo = new NodoD(info, this.cabecera.getSiguiente(), this.cabecera);
        //Referenciación:
        this.cabecera.setSiguiente(nuevo);
        nuevo.getSiguiente().setAnterior(nuevo);
        this.tamano++;
    }

    public void insertarFinal(T info) {
        //El anterior del nuevo nodo es el anterior de cabecera
        NodoD<T> nuevo = new NodoD(info, this.cabecera, this.cabecera.getAnterior());
        //El anterior de la cabeza es el nuevo nodo
        this.cabecera.setAnterior(nuevo);
        //El anterior del nuevo nodo su siguiente será nuevo nodo
        nuevo.getAnterior().setSiguiente(nuevo);
        //Aumentar cardinalidad
        this.tamano++;

    }

    public boolean esVacia() {
        // return this.tam==0
        return this.cabecera == this.cabecera.getSiguiente() && this.cabecera == this.cabecera.getAnterior();
    }

    /**
     * Elimina el nodo en la posición dada Comprobar que pos (posición) sea
     * válida  mayor 0 y mejor cardinalidad xxx
     * Buscar el nodo actual dada la posición  getPos(…) xxxx
     * Coloco nodo anterior = actual.getAnt() xxxx
     * 
     * nodoAnt su siguiente Es el siguiente de nodo actual xxx El siguiente de
     * nodo actual su anterior es nodoAnt Cardinilidad –
     *
     * @param pos: Posición del nodo
     * @return: El nodo eliminado
     */
    public T eliminar(int pos) {
        try {
            NodoD<T> nodoActual = this.getPos(pos);
            NodoD<T> nodoAnterior = nodoActual.getAnterior();
            nodoAnterior.setSiguiente(nodoActual.getSiguiente());
            nodoActual.getSiguiente().setAnterior(nodoAnterior);
            this.tamano--;
            this.desUnirNodo(nodoActual);
            return nodoActual.getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    private void desUnirNodo(NodoD<T> nodo) {
        nodo.setSiguiente(nodo);
        nodo.setAnterior(nodo);
    }

    /**
     * Obtiene la posición del nodo en la posición indicada
     *
     * @param pos: posición del nodo
     * @return: el nodo en la posición
     * @throws Exception: Si la posición no está dentro de los parámetros.
     */
    private NodoD<T> getPos(int pos) throws Exception {
        if (pos < 0 || pos > this.tamano) {
            throw new Exception("La posición " + pos + " no es válida.");
        }
        //Contribución de Carlos Eduardo Contreras Mendoza
        if (pos == this.tamano - 1) {
            return this.cabecera.getAnterior();
        }

        //Contribución de Carlos Eduardo Jeison Ferrer
        //EL METODO SE PRUEBA EL LA CLASE TestGetPostListaCD DE LA VISTA
        NodoD<T> nodoPos;
        if (pos >= this.tamano / 2) {
            pos = this.tamano - 1 - pos;
            nodoPos = this.cabecera.getAnterior();
            while (pos-- > 0) {
                nodoPos = nodoPos.getAnterior();
            }
        } else {
            nodoPos = this.cabecera.getSiguiente();
            while (pos-- > 0) {
                nodoPos = nodoPos.getSiguiente();
            }
        }
        return nodoPos;
    }
    
    /**
     * Obtiene la información del nodo a partir de la posición indicada
     *
     * @param pos: posición del nodo
     * @return Nodo con la información
     * @return: la info del nodo en la posición
     * @throws Exception: Si la posición no está dentro de los parámetros.
     */
    public T getPosInfo(int pos) throws Exception {
        if (pos < 0 || pos > this.tamano) {
            throw new Exception("La posición " + pos + " no es válida.");
        }
        //Contribución de Carlos Eduardo Contreras Mendoza
        if (pos == this.tamano - 1) {
            return this.cabecera.getAnterior().getInfo();
        }

        NodoD<T> nodoPos;
        if (pos >= this.tamano / 2) {
            pos = this.tamano - 1 - pos;
            nodoPos = this.cabecera.getAnterior();
            while (pos-- > 0) {
                nodoPos = nodoPos.getAnterior();
            }
        } else {
            nodoPos = this.cabecera.getSiguiente();
            while (pos-- > 0) {
                nodoPos = nodoPos.getSiguiente();
            }
        }
        return nodoPos.getInfo();
    }

    /**
     * Corta la lista a partir de la posición inicial hasta la final
     * incluyendose las posiciones Ejemplo: L=8,6,7,3,2,5 l2=L.cortar(1,3)
     * l2=6,7,3 y L=8,2,5
     * Condiciónes: - No crear nodos - posI menor o igual posF
     *
     * @param posI: posición inicial a tener en cuenta para eliminar
     * @param posF: posición final a tener en cuenta para eliminar
     * @return La lista con los elementos eliminados
     */
    public ListaCD<T> cortar(int posI, int posF) {
        if (posI <= posF) {
            try {
                ListaCD<T> l2 = new ListaCD<T>();

                NodoD<T> nodoI = this.getPos(posI);
                NodoD<T> nodoF = posI != posF ? this.getPos(posF) : nodoI;

                NodoD<T> nodoAI = nodoI.getAnterior();
                NodoD<T> nodoSF = nodoF.getSiguiente();
                nodoAI.setSiguiente(nodoSF);
                nodoSF.setAnterior(nodoAI);

                l2.cabecera.setSiguiente(nodoI);
                nodoI.setAnterior(l2.cabecera);
                l2.cabecera.setAnterior(nodoF);
                nodoF.setSiguiente(l2.cabecera);

                l2.tamano = (posF - posI) + 1;

                this.tamano = this.tamano - l2.tamano;

                return l2;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }

        return null;
    }

    /**
     * Verifica si mi lista es palíndrome
     *
     * @return si es o no palíndrome
     */
    public boolean esPalindrome() {
        if (this.tamano > 1) {
            // Tomo el siguiente y el anterior de mi cabecera para iterar y compararlos consecutivamente
            NodoD<T> nodo1 = this.cabecera.getSiguiente();
            NodoD<T> nodo2 = this.cabecera.getAnterior();
            // La iterración se hace solo sobre la mitad de la lista
            int c = this.tamano / 2;
            while (c-- > 0) {
                // Si la información no es igual, retorno false
                if (!nodo1.getInfo().equals(nodo2.getInfo())) {
                    return false;
                }
                // reasigno sus nuevos siguiente y anterior para comparar en la siguiente iteración.
                nodo1 = nodo1.getSiguiente();
                nodo2 = nodo2.getAnterior();
            }
        }

        return this.tamano != 0 ? true : false;
    }

    /**
     * Busca dentro de mi lista aquellos valores menores o iguales a la info y
     * los retorna
     *
     * @param info: La información a comparar
     * @return una lista con aquellos nodos encontrados
     */
    public ListaCD<T> getMenores(T info) {

        if (info != null && this.tamano > 0) {
            ListaCD<T> l2 = new ListaCD<T>();

            NodoD<T> nodo = this.cabecera.getSiguiente();
            int c = this.tamano;
            while (c-- > 0) {
                int menor = ((Comparable) nodo.getInfo()).compareTo(info);

                if (menor < 1) {
                    // al ser menor, su anterior y su siguiente se entrelazan.
                    NodoD<T> nodoA = nodo.getAnterior();
                    NodoD<T> nodoF = nodo.getSiguiente();
                    nodoA.setSiguiente(nodoF);
                    nodoF.setAnterior(nodoA);
                    this.tamano--;

                    // Se insertan al final de la nueva lista los nodos encontrados
                    // para que de esta forma, queden en el mismo orden que se encontraron
                    // Mi siguiente será la cabeza
                    nodo.setSiguiente(l2.cabecera);
                    // Mi anterior será el anterior de la cabeza
                    nodo.setAnterior(l2.cabecera.getAnterior());
                    // El siguiente del anterior que tenía mi cabeza, será el nuevo nodo
                    l2.cabecera.getAnterior().setSiguiente(nodo);
                    // El nuevo anterior de mi cabeza será el nuevo nodo
                    l2.cabecera.setAnterior(nodo);
                    l2.tamano++;

                    // Si es menor, su siguiente será el siguiente del actual
                    nodo = nodoF;
                } else {
                    nodo = nodo.getSiguiente();
                }
            }

            return l2;
        }
        return null;
    }

    @Override
    public String toString() {
        String msg = "";

        for (NodoD<T> x = this.cabecera.getSiguiente(); x != this.cabecera; x = x.getSiguiente()) {
            msg += x.getInfo().toString() + ",";
        }

        return msg;
    }

}
