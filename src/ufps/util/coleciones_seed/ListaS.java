/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {

    private Nodo<T> cabeza = null;
    private int cardinalidad = 0;

    public ListaS() {
    }

    /**
     * 1. Crear el nodo 2. Insertar el info en el nodo 3. El nuevo nodo su
     * siguiente es cabeza 4. Cabeza es ahora quién ? Nuevo nodo 5. aumentar
     * carnidalidad
     *
     * @param info que se desea almacenar
     */
    public void insertarInicio(T info) {
        // Paso 1.
        Nodo<T> nuevo = new Nodo();
        //Paso 2. 
        nuevo.setInfo(info);
        //Paso 3.
        nuevo.setSig(this.cabeza);
        //Paso 4.
        this.cabeza = nuevo;
        //Paso 5.
        this.cardinalidad++;
    }

    // arraylist x= .... , x.add(marco), x.add(juan) --> cabeza: marco
    // arraylist x= .... , x.add(juan),x.add(marco),  --> cabeza: juan   --> "método add"
    public int getCardinalidad() { //getTamanio o size()
        return cardinalidad;
    }

    @Override
    public String toString() {

        String msg = "";

        for (Nodo<T> posicion = this.cabeza; posicion != null; posicion = posicion.getSig()) {
            msg += posicion.getInfo().toString() + "->";
        }
        return msg + "null";

    }

    public boolean esta(T info) // containtTo(..)
    {
        for (Nodo<T> posicion = this.cabeza; posicion != null; posicion = posicion.getSig()) {
            if (posicion.getInfo().equals(info)) {
                return true;
            }
        }
        return false;
    }

    public void insertarFin(T info) {
        //1. Situación ocurre cuando la lista está vacía:
        if (this.esVacia()) {
            this.insertarInicio(info);
        } else {
            try {
                Nodo<T> ultimo = getPos(this.cardinalidad - 1);
                Nodo<T> nuevo = new Nodo(info, null);
                ultimo.setSig(nuevo);
                this.cardinalidad++;

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    /**
     * Decorador Inserta un dato al final de la lista , si y solo si, el dato no
     * está
     *
     * @param info el objeto que deseo insertar
     */
    public void insertarFin_NO_repetido(T info) {
        try {
            this.insertarFin_NO_repetido_excp(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    private void insertarFin_NO_repetido_excp(T info) {
        if (this.esta(info)) {
            //throw new RuntimeException("El dato no se puede insertar por que ya está registrado");
            return;
        }
        this.insertarFin(info);

    }

    public void insertarFin_NO_repetido_version2(T info) {
        if (this.esVacia()) {
            this.insertarInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            while (x.getSig() != null) {
                if (x.getInfo().equals(info) || x.getSig().getInfo().equals(info)) {
                    x = null;
                    throw new RuntimeException("El dato " + info + " no se puede insertar por que ya está registrado");
                } else {
                    x = x.getSig();
                }
            }
            if (x != null) {
                x.setSig(new Nodo(info, null));
                this.cardinalidad++;
            }
        }
    }

    /**
     * Obtiene un objeto almacenado en la lista a partir de su posición
     *
     * @param pos la posición dentro de la lista
     * @return información de la lista
     */
    public T get(int pos) {

        try {
            Nodo<T> x = this.getPos(pos);
            return x.getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }

    }

    /**
     * Cambia un objeto de la posición especificada
     *
     * @param pos indice del objeto a cambiar
     * @param objetoNuevo un objeto nuevo
     */
    public void set(int pos, T objetoNuevo) {

        try {
            Nodo<T> x = this.getPos(pos);
            x.setInfo(objetoNuevo);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }

    }

    private Nodo<T> getPos(int posFin) throws Exception {
        if (this.esVacia() || posFin < 0 || posFin > this.cardinalidad) {
            throw new Exception("No se encuentra la posición del nodo:" + posFin);
        }

        Nodo<T> x = this.cabeza;
        while (posFin > 0) {
            x = x.getSig();
            posFin--;
        }
        return x;
    }

    public boolean esVacia() {
        return this.cabeza == null;
    }

    /**
     * Método que elimina a traves de una posición dada
     *
     * @param pos: la posición del nodo a eliminar
     * @return el objeto eliminado
     */
    public T eliminar(int pos) {
        if (this.esVacia()) {
            return null;
        }

        Nodo<T> borrado;
        if (pos == 0) {
            borrado = this.cabeza;
            this.cabeza = this.cabeza.getSig();
            return borrado.getInfo();
        } else {
            try {
                // Obtengo el nodo anterior
                Nodo<T> antes = this.getPos(pos - 1);
                // Obtengo el nodo a borrar
                borrado = antes.getSig();
                // al nodo anterior le asigno el nodo siguiente al borrado
                antes.setSig(borrado.getSig());

                return borrado.getInfo();
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
        }
    }

    /**
     * Método que cambia de posición un nodo
     *
     * @param info: Nodo a mover
     */
    public void cortarDato_pasarlo_Al_Final(T info) {
        if (this.esVacia()) {
            return;
        }
        /**
         * La posición {0} almacena el nodo anterior a la info, La posición {1}
         * almacena el nodo a mover, La posición {2} almacena el último nodo
         */
        Nodo<T>[] p = this.getPosiciones(info);

        // Si p[1] info está y al encontrarse el último nodo p[2], el nuevo último será p[1]
        if (p[1] != null) {
            // Si p[0] es null es porque el nodo era la cabeza
            if (p[0] != null) {
                p[0].setSig(p[1].getSig());
            }
            // {p[2]} es el último nodo, su siguiente será info {p[1]}
            p[2].setSig(p[1]);
            p[1].setSig(null);
        }
    }

    private Nodo<T>[] getPosiciones(T info) {
        // vector-> De tres posiciones: <anterior, actual, último>
        Nodo<T>[] p = new Nodo[3];
        // Si la info es la cabeza, entonces su siguiente será la nueva cabeza
        if (this.cabeza.getInfo().equals(info) && this.cabeza.getSig() != null) {
            p[1] = this.cabeza;
            this.cabeza = this.cabeza.getSig();
        }

        Nodo<T> u = this.cabeza;
        // Busco en nodo siguiente hasta encontrar el último
        while (u.getSig() != null) {
            // Si el nodo siguiente de u {u.getSig()} es igual a la info, el actual se asume como el anterior = {u} 
            // Validamos que exista un siguiente de la info para descartar que la info no sea el último nodo = {u.getSig().getSig()}
            if (u.getSig().getInfo().equals(info) && u.getSig().getSig() != null) {
                p[0] = u; // u es el anterior al nodo a mover
                p[1] = u.getSig(); // nodo a mover
            }
            // asígnamos el siguiente para iterar y para llegar al último
            u = u.getSig();
        }

        p[2] = u;

        return p;
    }
    
    private void cambiarInfoNodos(Nodo<T> nodo) {
        T infoTemp = nodo.getInfo();
        nodo.setInfo(nodo.getSig().getInfo());
        nodo.getSig().setInfo(infoTemp);
    }
    
    private void cambiarNodosInfo(int n) {
        Nodo<T> x = this.cabeza;
        int p = n;
        while (p > 1) {
            x = x.getSig();
            p--;
        }
        System.out.println("*******N[" + n + "] = [" + x.getInfo() + "] ********");
        System.out.println(x.getSig().getInfo() + " < " + x.getInfo());
        int c = ((Comparable) x.getSig().getInfo()).compareTo(x.getInfo());
        if (c < 0) {
            this.cambiarInfoNodos(x);
            System.out.println("________ " + this.toString());
            n = n - 1;
            if (n > 0) {
                this.cambiarNodosInfo(n);
            }
        }
    }

    public void ordenarInsercion_Por_Infos() {
        Nodo<T> u = this.cabeza;

        int p = 1;
        while (u.getSig() != null) {
            int c = ((Comparable) u.getSig().getInfo()).compareTo(u.getInfo());
            System.out.println("P: " + p);
            System.out.println(u.getSig().getInfo() + " < " + u.getInfo());
            if (c < 0) {
                this.cambiarInfoNodos(u);
                System.out.println("Nuevo: " + this.toString());
                if (p > 1) {
                    int n = p-1;
                    this.cambiarNodosInfo(n);
                }
            }
            p++;
            u = u.getSig();
            System.out.println(this.toString());
            System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        }

    }
    
    /**
     * Cambia la cabeza de la lista con su sucesor y este es menor
     * @param nodo1: nodo a cambiar
     */
    private void cambiarNodosCabeza(Nodo<T> nodo1) {
        Nodo<T> nodo2 = nodo1.getSig();
        Nodo<T> nodo3 = nodo2.getSig();
        
        nodo1.setSig(nodo3);
        nodo2.setSig(nodo1);
        if(nodo1.getInfo().equals(this.cabeza.getInfo())) {
            this.cabeza = nodo2;
        }
    }
    
    /**
     * Intercambia los nodos intermedios de una lista, nodo2 pasa a ser nodo3 y nodo3 = nodo2
     * Para ello, nodo 1 debe apuntar a nodo3, nodo3 a nodo2 y nodo 2 al que apunto nodo3
     * @param nodo1: Nodo a cambiar
     */
    private void cambiarNodos(Nodo<T> nodo1) {
        Nodo<T> nodo2 = nodo1.getSig();
        Nodo<T> nodo3 = nodo2.getSig();
        Nodo<T> nodo4 = nodo3.getSig();
        
        nodo1.setSig(nodo3);
        nodo2.setSig(nodo4);
        nodo3.setSig(nodo2);
    }
    
    private Nodo<T>[] getNodoPos(T info) {
        // vector-> De dos posiciones: <anterior, actual>
        Nodo<T>[] p = new Nodo[2];
        // Si la info es la cabeza, su siguiente será el nodo a comparar para cambiar
        if (this.cabeza.getInfo().equals(info) && this.cabeza.getSig() != null) {
            p[1] = this.cabeza;
        } else {
            Nodo<T> u = this.cabeza;
            // Busco en nodo igual a la info partiendo desde su antecesor
            while (u.getSig() != null) {
                // Al encontrar el nodo, tomo su antecesor
                if (u.getSig().getInfo().equals(info)) {
                    p[0] = u; // u es el anterior al nodo a mover si es mayor
                    p[1] = u.getSig(); // nodo a mover si es menor
                }
                // asígnamos el siguiente para iterar y para llegar al último
                u = u.getSig();
            }
        }
        return p;
    }
    
    private void intercambiarNodos(T info) {
        // vector-> De dos posiciones: <anterior, actual>
        Nodo<T>[] pos = this.getNodoPos(info);
        int c;
        // Si el nodo se encuentra y tiene un siguiete, comparo para ver si este es mayor
        if(pos[1] != null && pos[1].getSig() != null)
            c = ((Comparable) pos[1].getSig().getInfo()).compareTo(pos[1].getInfo());
        else // Esto es para validar la última posición, en esta ya no se hace hacia adelante sino con el de atrás.
            c = ((Comparable) pos[0].getSig().getInfo()).compareTo(pos[0].getInfo());
        
        if (c < 0) {
            // Si no es la cabeza y la cola de la lista, los nodos a tener en cuenta con 4
            if (pos[0] != null && pos[1].getSig() != null) {
                this.cambiarNodos(pos[0]);
                System.out.println("_____________ " + this.toString());
                
                this.intercambiarNodos(pos[0].getInfo());
            } else if(pos[1] != null && pos[1].getSig() != null) {
                // Si es la cabeza, tres
                this.cambiarNodosCabeza(pos[1]);
            } else if(pos[0] != null && pos[1] != null) {
                // Si es la cola, solo vuelvo a llamar la función con su antecesor
                // para que ingrese al primer condicional
                this.intercambiarNodos(pos[0].getInfo());
            }
        }
    }
    
    /**
     * Ordena la lista por nodos de acuerdo a su información
     */
    public void ordenarInsercion_Por_Nodos() {
        Nodo<T> u = this.cabeza;
        int p = 1;
        while (p < this.cardinalidad) {
            T info = u.getInfo();
            // ejecuto una función que se puede volver recursiva
            this.intercambiarNodos(info);
            
            u = u.getSig();
            p++;
            System.out.println(this.toString());
            System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        }
    }
}
